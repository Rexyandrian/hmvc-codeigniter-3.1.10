# HMVC Codeigniter-3.1.10
* HMVC menggunakan Codeigniter-3.1.10 memudahkan kita dalam membangun website yang banyak menu nya. 
* **NB** : TIDAK ADA ATURAN BAKU PENGGUNAAN. DISINI TAK JELASKAN SEBAGAI DOKUMEN PRIBADI SAJA

## application/module
* anda bisa melakukan perantara ke database
* nama controller & model nya `terserah` biasanya saya menggunakan
    * controller `mod_app` atau `mod_model`

## application/panel
Digunakan untuk membuat menu contohnya disini kita akan membuat menu `user` maka proses nya :
*  buat folder `user` (rquired)
*  buat juga `user/controllers` (required)
*  jika akan menggunakan model dan view di folder user bisa ditambahkan `user/models` / `user/view` (optional)

saya biasanya model ditaruh di `application/module` sedangkan view tetep terpusah di `application/view` silahkan mana saja bisa 

## catatan
* semua controller di `panel` / `module` extends nya ke `MX_controller` agar bisa menggunakan HMVC


## How to install
* Download / clone this projek
* Taruh di htdocs (xampp) atau web server lain nya
* coba di url buka `http://yourdomain/contoh` and `http://yourdomain/contoh/aku` jika tampil sesuatu maka Anda berhasil install.
* done. tinggal digunakan